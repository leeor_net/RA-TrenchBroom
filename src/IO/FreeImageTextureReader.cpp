/*
 Copyright (C) 2010-2016 Kristian Duske

 This file is part of TrenchBroom.

 TrenchBroom is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 TrenchBroom is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with TrenchBroom. If not, see <http://www.gnu.org/licenses/>.
 */

#include "FreeImageTextureReader.h"

#include "Color.h"
#include "FreeImage.h"
#include "StringUtils.h"
#include "Assets/Texture.h"
#include "IO/CharArrayReader.h"
#include "IO/Path.h"

#include <cstring>

using namespace TrenchBroom;
using namespace TrenchBroom::IO;

FreeImageTextureReader::FreeImageTextureReader(const NameStrategy& nameStrategy) : TextureReader(nameStrategy)
{}


Assets::Texture* FreeImageTextureReader::doReadTexture(const char* const begin, const char* const end, const Path& path, float scale_w, float scale_h) const
{
	const size_t                imageSize = static_cast<size_t>(end - begin);
	BYTE*                       imageBegin = reinterpret_cast<BYTE*>(const_cast<char*>(begin));
	FIMEMORY*                   imageMemory = FreeImage_OpenMemory(imageBegin, static_cast<DWORD>(imageSize));
	const FREE_IMAGE_FORMAT     imageFormat = FreeImage_GetFileTypeFromMemory(imageMemory);
	FIBITMAP*                   image = FreeImage_LoadFromMemory(imageFormat, imageMemory);

	const String                imageName = path.filename();
	const size_t                imageWidth = static_cast<size_t>(FreeImage_GetWidth(image));
	const size_t                imageHeight = static_cast<size_t>(FreeImage_GetHeight(image));
	const FREE_IMAGE_COLOR_TYPE imageColourType = FreeImage_GetColorType(image);

	// Don't care about alpha channels, reduce to 24bit.
	if (imageColourType != FIC_RGB)
	{
		FIBITMAP* tempImage = FreeImage_ConvertTo24Bits(image);
		FreeImage_Unload(image);
		image = tempImage;
	}

	int imgResizedWidth = static_cast<int>(imageWidth * scale_w);
	int imgResizedHeight = static_cast<int>(imageHeight * scale_h);

	/*
	 *	Some specialized textures are set at 32x32 and shouldn't be scaled (sky, trigger, etc.)
	 */
	if (imageWidth <= 32 && imageHeight <= 32)
	{
		imgResizedWidth = imageWidth;
		imgResizedHeight = imageHeight;
	}

	FIBITMAP* imgResized = FreeImage_Rescale(image, imgResizedWidth, imgResizedHeight, FILTER_BILINEAR);

	// Flip otherwise textures appear upside down.
	FreeImage_FlipVertical(imgResized);

	// Textures are way too bright for whatever reason so darken them a bit.
	FreeImage_AdjustBrightness(imgResized, -50.0f);

	const size_t size = 3 * (imgResizedWidth * imgResizedHeight); //24bit images only have 3 bytes per pixel
	assert(size > 0);

	Assets::TextureBuffer buffer = Assets::TextureBuffer(size);
	std::memcpy(buffer.ptr(), FreeImage_GetBits(imgResized), buffer.size());

	FreeImage_Unload(imgResized);
	FreeImage_Unload(image);

	FreeImage_CloseMemory(imageMemory);

	return new Assets::Texture(textureName(imageName, path), imgResizedWidth, imgResizedHeight, Color(), buffer, GL_BGR);
}
