/*
 Copyright (C) 2010-2017 Kristian Duske
 
 This file is part of TrenchBroom.
 
 TrenchBroom is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 TrenchBroom is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with TrenchBroom. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "StringUtils.h"
#include "IO/MappedFile.h"
#include "IO/Path.h"

#include <memory>
#include <vector>

namespace TrenchBroom {
	class Logger;

	namespace Assets
	{
		class TextureCollection;
		class TextureReader;
		class TextureManager;

	}

	namespace IO {
	class FileSystem;
	class TextureReader;

	class TextureCollectionLoader
	{
	public:
		typedef std::unique_ptr<TextureCollectionLoader> Ptr;

	public:
		virtual ~TextureCollectionLoader();

		Assets::TextureCollection* loadTextureCollection(const Path& path, const String& textureExtension, const TextureReader& textureReader);

	protected:
		typedef std::pair<float, float> Point2D;
		typedef std::vector<Point2D> Point2DList;

	protected:
		TextureCollectionLoader();

	private:
		virtual MappedFile::List doFindTextures(const Path& path, const String& extension, Point2DList& pt_list) = 0;
	};


	class DirectoryTextureCollectionLoader : public TextureCollectionLoader
	{
	public:
		DirectoryTextureCollectionLoader(const FileSystem& gameFS);

	private:
		MappedFile::List doFindTextures(const Path& path, const String& extension, Point2DList& pt_list);

		void parseMaterial(Path::List& paths, Point2DList& pt_list);

	private:
		const FileSystem& m_gameFS;
	};

}
}
