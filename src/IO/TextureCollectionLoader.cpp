/*
 Copyright (C) 2010-2017 Kristian Duske
 
 This file is part of TrenchBroom.
 
 TrenchBroom is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 TrenchBroom is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with TrenchBroom. If not, see <http://www.gnu.org/licenses/>.
 */

#include "TextureCollectionLoader.h"

#include "Assets/AssetTypes.h"
#include "Assets/TextureCollection.h"
#include "Assets/TextureManager.h"
#include "IO/DiskIO.h"
#include "IO/FileMatcher.h"
#include "IO/FileSystem.h"
#include "IO/TextureReader.h"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <memory>

using namespace TrenchBroom;
using namespace IO;

TextureCollectionLoader::TextureCollectionLoader()
{}


TextureCollectionLoader::~TextureCollectionLoader()
{}


Assets::TextureCollection* TextureCollectionLoader::loadTextureCollection(const Path& path, const String& textureExtension, const TextureReader& textureReader)
{
	std::unique_ptr<Assets::TextureCollection> collection(new Assets::TextureCollection(path));

	Point2DList textureScale;
	MappedFile::List file_list = doFindTextures(path, textureExtension, textureScale);

	assert(file_list.size() == textureScale.size());

	for (size_t i = 0; i < file_list.size(); ++i)
	{
		Assets::Texture* texture = textureReader.readTexture(file_list[i]->begin(), file_list[i]->end(), file_list[i]->path(), textureScale[i].first, textureScale[i].second);
		collection->addTexture(texture);
	}

	return collection.release();
}


DirectoryTextureCollectionLoader::DirectoryTextureCollectionLoader(const FileSystem& gameFS) :
	m_gameFS(gameFS)
{}


/**
 * Parses through a '.material' script and replaces the material filename with
 * the diffuse texture filename instead.
 * 
 * \note	Some materials do not define a diffuse texture -- in these cases this
 *			function will try to look for a '.tga' file instead of the material
 *			script. If this '.tga' doesn't exist an exception will be thrown.
 */
void DirectoryTextureCollectionLoader::parseMaterial(Path::List& paths, Point2DList& pt_list)
{
	// Parse through material script
	for (size_t i = 0; i < paths.size(); ++i)
	{
		MappedFile::Ptr _ptr = m_gameFS.openFile(paths[i]);

		std::string buff(_ptr->begin(), _ptr->size());
		StringList script_lines = StringUtils::split(buff, '\n');

		// Some text editors add a windows style line ending '\r\n' -- in this case the above split
		// will leave trailing '\r' characters at the end of the token list so go through and remove.
		for (size_t i = 0; i < script_lines.size(); ++i)
		{
			if (script_lines[i].find('\r') != std::string::npos)
				script_lines[i] = script_lines[i].substr(0, script_lines[i].size() - 1);
		}

		// Remove all empty lines
		for (size_t i = 0; i < script_lines.size(); ++i)
		{
			if (script_lines[i].empty())
			{
				script_lines.erase(script_lines.begin() + i);
				--i;
			}
		}

		// Look for a diffuse texture definition
		String diffuseTexture;
		Point2D _scale(1.0f, 1.0f);
		for (auto line : script_lines)
		{
			if (line.find("texture_diffuse") != String::npos)
			{
				StringList tokens = StringUtils::split(line, ' ');
				if (tokens.size() < 2)
					throw std::runtime_error("DirectoryTextureCollectionLoader::doFindTextures(): Invalid diffuse texture declartaion in '" + paths[i].lastComponent().filename());

				diffuseTexture = tokens[1]; // Risky?
			}

			if (line.find("texture_scale") != String::npos)
			{
				StringList tokens = StringUtils::split(line, ' ');
				if (tokens.size() < 3)
					throw std::runtime_error("DirectoryTextureCollectionLoader::doFindTextures(): Invalid texture scale declartaion in '" + paths[i].lastComponent().filename());

				_scale.first = std::stof(tokens[1]);
				_scale.second = std::stof(tokens[2]);
			}
		}

		String fullpath = paths[i].asString();
		if (diffuseTexture.empty())
		{
			fullpath = fullpath.substr(0, fullpath.rfind("."));
			fullpath = fullpath + ".tga";
			if (!m_gameFS.fileExists(Path(fullpath)))
				throw std::runtime_error("DirectoryTextureCollectionLoader::doFindTextures(): Unable to find a suitable texture for \n\n" + paths[i].asString());
		}
		else
		{
			fullpath = fullpath.substr(0, fullpath.rfind(paths[0].separator()) + 1);
			fullpath += diffuseTexture;
		}

		paths[i] = Path(fullpath);
		pt_list.push_back(_scale);
	}
}


MappedFile::List DirectoryTextureCollectionLoader::doFindTextures(const Path& path, const String& extension, Point2DList& pt_list)
{
	Path::List paths = m_gameFS.findItems(path, FileExtensionMatcher(extension));

	parseMaterial(paths, pt_list);

	MappedFile::List result;
	result.reserve(paths.size());

	std::transform(std::begin(paths), std::end(paths), std::back_inserter(result),
		[this](const Path& filePath) { return m_gameFS.openFile(filePath); });

	return result;
}
