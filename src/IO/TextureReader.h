/*
 Copyright (C) 2010-2017 Kristian Duske
 
 This file is part of TrenchBroom.
 
 TrenchBroom is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 TrenchBroom is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with TrenchBroom. If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include "Macros.h"
#include "Assets/AssetTypes.h"
#include "IO/MappedFile.h"

namespace TrenchBroom {
namespace IO {

	class Path;

	class TextureReader
	{
	public:
		class NameStrategy
		{
		public:
			virtual ~NameStrategy();
			NameStrategy* clone() const;
			String textureName(const String& textureName, const Path& path) const;

		protected:
			NameStrategy();

		private:
			virtual NameStrategy* doClone() const = 0;
			virtual String doGetTextureName(const String& textureName, const Path& path) const = 0;

			deleteCopyAndAssignment(NameStrategy)
		};


		class TextureNameStrategy : public NameStrategy
		{
		public:
			TextureNameStrategy();

		private:
			NameStrategy* doClone() const;
			String doGetTextureName(const String& textureName, const Path& path) const;

			deleteCopyAndAssignment(TextureNameStrategy)
		};


		class PathSuffixNameStrategy : public NameStrategy
		{
		public:
			PathSuffixNameStrategy(size_t suffixLength, bool deleteExtension);

		private:
			NameStrategy* doClone() const;
			String doGetTextureName(const String& textureName, const Path& path) const;

			deleteCopyAndAssignment(PathSuffixNameStrategy)

		private:
			size_t m_suffixLength;
			bool m_deleteExtension;
		};

	protected:
		TextureReader(const NameStrategy& nameStrategy);

	public:
		virtual ~TextureReader();

		Assets::Texture* readTexture(MappedFile::Ptr file, float scale_w, float scale_h) const;
		Assets::Texture* readTexture(const char* const begin, const char* const end, const Path& path, float scale_w, float scale_h) const;

	protected:
		String textureName(const String& textureName, const Path& path) const;

	private:
		virtual Assets::Texture* doReadTexture(const char* const begin, const char* const end, const Path& path, float scale_w, float scale_h) const = 0;

	private:
		NameStrategy* m_nameStrategy;

		deleteCopyAndAssignment(TextureReader)
	};

}
}
