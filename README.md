# RA-TrenchBroom

RA-TrenchBroom is a modified version of TrenchBroom designed for use with Rogue Arena, a LairWorks Entertainment production.

## Differences

TrenchBroom is a cross-game editor that allows map editing for Quake, Quake 2, Hexen and several other idTech engines.

RA-TrenchBroom is a modified version of TrenchBroom that strips out support for other game types and is based on idTech 2 engine properties.

RA-TrenchBroom also adds several GUI elements to make it easier for users coming from Qoole type editors to jump right in.

Lastly, support for older texture formats has been removed in favor of the modern image types used in Rogue Arena.

## Releases

### v1.0.2 04/12/2017

* Improved texture loading -- textures no longer appear too bright.
* Fixed assumption in Md2Model::Frame::Frame() that verticies would always be valid which would lead to an exception being thrown.

### v1.0.1 28/06/2017

* MD2's without a correct texture path will now load anyway.
* Removed .PCX texture loading -- MD2's specifying a .PCX skin will now instead attempt to load a .TGA -- if none exists a blank skin will be created.
* Fixed an issue in texture loading that would cause textures to be drawn upside down.

### v1.0.0 28/06/2017

Initial version of RA-TrenchBroom.

* Loading and processing of '.material' scripts as used in the Rogue Arena engine -- loads and appropriately scales .TGA textures.
* Added a 'delete' button to allow editors to use the mouse to delete brushes/entities.
* Tailored to Rogue Arena dropping support for other games.
* Removed support for loading BSP files directly (this is nonsensical for Rogue Arena).
* Added tooltips to MapView2D to assist designers in knowing which views they're working in.
* Added MapType flag to worldspawn -- this is a tag that will be used by the RogueArena engine for sorting/filtering maps in the game selection screens.
* Updated and tailored the entity definition file (FGD) for RogueArena.

## Compiling

RA-TrenchBroom is developed on Windows and as such only has Visual Studio official project files. Most dependencies are included as source files with the code so building should be fairly straight forward.

# Credits
- Based on TrenchBroom (http://kristianduske.com/trenchbroom)


# License

RA-TrenchBroom is licensed under GPL3. See GPL.TXT for specifics.